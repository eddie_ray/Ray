/*    */ package com.ray.util;
/*    */ 
/*    */ import java.util.Calendar;
/*    */ 
/*    */ public class Commen
/*    */ {
/*    */   public static String getLoginTimes()
/*    */   {
/*    */     try
/*    */     {
/* 11 */       return com.jfinal.plugin.redis.Redis.use("test").getCounter("loginTimes").toString();
/*    */     } catch (Exception e) {}
/* 13 */     return "0";
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public static String getThisMonthFirstDay()
/*    */   {
/* 20 */     java.text.SimpleDateFormat sm = new java.text.SimpleDateFormat("yyyyMMdd");
/* 21 */     Calendar calstr = Calendar.getInstance();
/* 22 */     calstr.add(2, 0);
/* 23 */     calstr.set(5, 1);
/* 24 */     String first = sm.format(calstr.getTime());
/* 25 */     return first;
/*    */   }
/*    */   
/*    */   public static String getThisMonthLastDay() {
/* 29 */     java.text.SimpleDateFormat sm = new java.text.SimpleDateFormat("yyyyMMdd");
/* 30 */     Calendar calast = Calendar.getInstance();
/* 31 */     calast.set(5, calast.getActualMaximum(5));
/* 32 */     String last = sm.format(calast.getTime());
/* 33 */     return last;
/*    */   }
/*    */ }
