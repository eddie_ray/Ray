package com.ray.common.controller;

import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Record;

public class BaseController extends Controller{

	@NotAction
	protected Integer getSessionUserId() {
		Record user = (Record) getSessionAttr("user");
		return user.getInt("id");
	}
}