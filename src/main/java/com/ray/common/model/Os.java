package com.ray.common.model;

import java.util.List;

public class Os {
	
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	private String label;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	private List<Os> children;
	public List<Os> getChildren() {
		return children;
	}
	public void setChildren(List<Os> children) {
		this.children = children;
	}
}
