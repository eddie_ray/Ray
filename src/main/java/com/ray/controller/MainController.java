/*     */ package com.ray.controller;

import com.dingtalk.api.response.OapiUserGetResponse;
/*     */
/*     */ import com.jfinal.plugin.activerecord.Db;
/*     */ import com.jfinal.plugin.activerecord.Record;
/*     */ import com.jfinal.plugin.redis.Redis;
/*     */ import com.ray.common.controller.BaseController;
/*     */ import java.util.List;
/*     */ import org.apache.shiro.SecurityUtils;
/*     */ import org.apache.shiro.authc.ExcessiveAttemptsException;
/*     */ import org.apache.shiro.authc.IncorrectCredentialsException;
/*     */ import org.apache.shiro.authc.UnknownAccountException;
/*     */ import org.apache.shiro.authc.UsernamePasswordToken;
/*     */ import org.apache.shiro.subject.Subject;

/*     */
/*     */ public class MainController extends BaseController
/*     */ {
	/*     */ public void index()
	/*     */ {
		/* 20 */ setAttr("menu", getMenu());
		/*     */
		/* 22 */ render("index.html");
		/*     */ }

	/*     */
	/*     */ public void loginInit() {
				if(getPara("code")!=null){
					setAttr("code", getPara("code"));
					setAttr("icon", getPara("icon"));
				}else{
					setAttr("code", 0);
					setAttr("icon", 1);
				}
		/* 26 */ render("login.html");
		/*     */ }

	/*     */
	/*     */ public void login() {
		/* 30 */ UsernamePasswordToken token = new UsernamePasswordToken(getPara("username"), getPara("password"));
		/* 31 */ Subject subject = SecurityUtils.getSubject();
		/* 32 */ Record rsp = new Record();
		/* 33 */ rsp.set("code", Integer.valueOf(0));
		/*     */ try {
			/* 35 */ subject.login(token);
			/* 36 */ rsp.set("result", Integer.valueOf(1));
			/* 37 */ rsp.set("msg", "登录成功");
			/* 38 */ rsp.set("icon", Integer.valueOf(1));
			/*     */ }
		/*     */ catch (IncorrectCredentialsException ice) {
			/* 41 */ rsp.set("result", Integer.valueOf(0));
			/* 42 */ rsp.set("msg", "密码错误");
			/* 43 */ rsp.set("icon", Integer.valueOf(5));
			/* 44 */ renderJson(rsp);
			/* 45 */ return;
			/*     */ }
		/*     */ catch (UnknownAccountException uae) {
			/* 48 */ rsp.set("result", Integer.valueOf(0));
			/* 49 */ rsp.set("msg", "用户不存在");
			/* 50 */ rsp.set("icon", Integer.valueOf(5));
			/* 51 */ renderJson(rsp);
			/* 52 */ return;
			/*     */ }
		/*     */ catch (ExcessiveAttemptsException eae) {
			/* 55 */ rsp.set("result", Integer.valueOf(0));
			/* 56 */ rsp.set("msg", "错误登录过多");
			/* 57 */ rsp.set("icon", Integer.valueOf(5));
			/* 58 */ renderJson(rsp);
			/* 59 */ return;
			/*     */ }
		/* 61 */ Record user = Db.findFirst("select * from user where username = '" + getPara("username") + "'");
		/* 62 */ subject.getSession().setAttribute("user", user);
		/*     */ try {
			/* 64 */ Redis.use("test").incr("loginTimes");
			/*     */ }
		/*     */ catch (Exception localException) {
		}
		/*     */
		/* 68 */ renderJson(rsp);
		/*     */ }

	/*     */
	/*     */ public void logout() {
		/* 72 */ Subject currentUser = SecurityUtils.getSubject();
		/* 73 */ currentUser.logout();
		setAttr("code", 0);
		setAttr("icon", 1);
		/* 74 */ render("login.html");
		/*     */ }

	/*     */
	/*     */ public void console() {
		/* 78 */ render("home/console.html");
		/*     */ }

	/*     */
	/*     */
	/*     */
	/*     */
	/*     */ public List<Record> getMenu()
	/*     */ {
		/* 86 */ List<Record> top_menu = Db
				.find("select * from menu where menu_level = 1 and is_hide = 0 order by seq_num");
		/* 87 */ top_menu = menuAuth(top_menu);
		/* 88 */ for (int i = 0; i < top_menu.size(); i++) {
			/* 89 */ List<Record> second_menu = Db
					.find("select * from menu where menu_level = 2 and is_hide = 0 and parent_menu = "
							+ ((Record) top_menu.get(i)).getInt("id") + " order by seq_num");
			/* 90 */ second_menu = menuAuth(second_menu);
			/* 91 */ for (int j = 0; j < second_menu.size(); j++) {
				/* 92 */ List<Record> third_menu = Db
						.find("select * from menu where menu_level = 3 and is_hide=0 and parent_menu = "
								+ ((Record) second_menu.get(j)).getInt("id") + " order by seq_num");
				/* 93 */ third_menu = menuAuth(third_menu);
				/* 94 */ if (third_menu.size() > 0) {
					/* 95 */ ((Record) second_menu.get(j)).set("children", third_menu);
					/*     */ }
				/*     */ }
			/* 98 */ if (second_menu.size() > 0) {
				/* 99 */ ((Record) top_menu.get(i)).set("children", second_menu);
				/*     */ }
			/*     */ }
		/* 102 */ return top_menu;
		/*     */ }

	/*     */
	/*     */
	/*     */
	/*     */
	/*     */ public List<Record> menuAuth(List<Record> menu)
	/*     */ {
		/* 110 */ OapiUserGetResponse user = (OapiUserGetResponse) getSessionAttr("user");
		/* 111 */ String sql = "SELECT gl_id FROM permissions WHERE id IN (SELECT permission_id FROM role_permission WHERE role_id IN (SELECT role_id FROM user_role WHERE user_id = "
				+ user.getUserid() + ")) AND TYPE = 1";
		/* 112 */ List<Record> menuPermissions = Db.find(sql);
		/* 113 */ for (int i = 0; i < menu.size(); i++) {
			/* 114 */ boolean flag = true;
			/* 115 */ for (int j = 0; j < menuPermissions.size(); j++) {
				/* 116 */ if (((Record) menu.get(i)).getInt("id") == ((Record) menuPermissions.get(j))
						.getInt("gl_id")) {
					/* 117 */ flag = false;
					/*     */ }
				/*     */ }
			/* 120 */ if (flag) {
				/* 121 */ menu.remove(i);
				/* 122 */ i--;
				/*     */ }
			/*     */ }
		/* 125 */ return menu;
		/*     */ }

	/*     */
	/*     */ public static void main(String[] args) {
		/* 129 */ Redis.use("test").lpush("ray", new Object[] { "1" });
		/*     */ }
	/*     */ }