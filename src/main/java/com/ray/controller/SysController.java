/*     */ package com.ray.controller;
/*     */ 
/*     */ import com.jfinal.aop.Before;
/*     */ import com.jfinal.plugin.activerecord.Db;
/*     */ import com.jfinal.plugin.activerecord.Page;
/*     */ import com.jfinal.plugin.activerecord.Record;
/*     */ import com.jfinal.plugin.activerecord.SqlPara;
/*     */ import com.jfinal.plugin.activerecord.tx.Tx;
/*     */ import com.ray.common.controller.BaseController;
/*     */ import com.ray.common.model.Menu;
/*     */ import java.util.List;
/*     */ 
/*     */ public class SysController extends BaseController
/*     */ {
/*     */   public void user()
/*     */   {
/*  17 */     render("user.html");
/*     */   }
/*     */   
/*     */ 
/*     */   public void getUser()
/*     */   {
/*  23 */     String username = getPara("username");
/*  24 */     String nickname = getPara("nickname");
/*  25 */     String strSql = "select * from user where 1=1";
/*  26 */     if ((username != null) && (!"".equals(username))) {
/*  27 */       strSql = strSql + " and username like '%" + username + "%'";
/*     */     }
/*  29 */     if ((nickname != null) && (!"".equals(nickname))) {
/*  30 */       strSql = strSql + " and nickname like '%" + nickname + "%'";
/*     */     }
/*  32 */     SqlPara sql = new SqlPara();
/*  33 */     sql.setSql(strSql);
/*  34 */     Page<Record> users = Db.paginate(getParaToInt("page").intValue(), getParaToInt("limit").intValue(), sql);
/*  35 */     List<Record> userList = users.getList();
/*     */     
/*  37 */     for (int i = 0; i < userList.size(); i++) {
/*  38 */       StringBuffer roles = new StringBuffer();
/*  39 */       List<Record> roleIds = Db.find("select role_id from user_role where user_id = " + ((Record)userList.get(i)).get("id"));
/*  40 */       for (int j = 0; j < roleIds.size(); j++) {
/*  41 */         Record role = Db.findById("roles", ((Record)roleIds.get(j)).get("role_id"));
/*  42 */         roles.append(role.getStr("role_nick_name"));
/*  43 */         roles.append(",");
/*     */       }
/*  45 */       ((Record)userList.get(i)).set("roles", roles.toString().substring(0, roles.toString().length() - 1));
/*     */     }
/*  47 */     Record layTableRes = new Record();
/*  48 */     layTableRes.set("code", Integer.valueOf(0));
/*  49 */     layTableRes.set("msg", "");
/*  50 */     layTableRes.set("count", Integer.valueOf(users.getTotalRow()));
/*  51 */     layTableRes.set("data", users.getList());
/*  52 */     renderJson(layTableRes);
/*     */   }
/*     */   
/*     */   public void userform() {
/*  56 */     int userId = getParaToInt(0).intValue();
/*  57 */     setAttr("user", new Record().set("id", Integer.valueOf(0)));
/*  58 */     if (userId != 0) {
/*  59 */       Record user = Db.findById("user", Integer.valueOf(userId));
/*  60 */       setAttr("user", user);
/*  61 */       Record roles = Db.findFirst("SELECT GROUP_CONCAT(role_id) role_ids FROM user_role WHERE user_id = '" + userId+"'");
/*  62 */       setAttr("roleIds", roles.get("role_ids"));
/*     */     }
/*  64 */     render("userform.html");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void getSelectRole()
/*     */   {
/*  71 */     List<Record> roles = Db.find("select id,role_nick_name name from roles");
/*  72 */     renderJson(roles);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   @Before({Tx.class})
/*     */   public void userUpdate()
/*     */   {
/*  80 */     String userId = getPara("userid");
/*     */     
/*  94 */     Db.delete("delete from user_role where user_id = '" + userId+"'");
/*     */     
/*  96 */     String[] roleIdArray = getPara("rolesId").split(",");
/*  97 */     for (int i = 0; i < roleIdArray.length; i++) {
/*  98 */       Record user_role = new Record();
/*  99 */       user_role.set("role_id", roleIdArray[i]);
/* 100 */       user_role.set("user_id", userId);
/* 101 */       Db.save("user_role", user_role);
/*     */     }
/* 103 */     Record rsp = new Record();
/* 104 */     rsp.set("code", Integer.valueOf(0));
/* 105 */     rsp.set("msg", "员工角色修改成功");
/* 106 */     renderJson(rsp);
/*     */   }
/*     */   
/*     */   public void role() {
/* 110 */     render("role.html");
/*     */   }
/*     */   
/*     */   public void roleform() {
/* 114 */     int roleId = getParaToInt(0).intValue();
/* 115 */     setAttr("role", new Record().set("id", Integer.valueOf(0)));
/* 116 */     if (roleId != 0) {
/* 117 */       Record role = Db.findById("roles", Integer.valueOf(roleId));
/* 118 */       setAttr("role", role);
/*     */     }
/* 120 */     render("roleform.html");
/*     */   }
/*     */   
/*     */   public void getRole() {
/* 124 */     List<Record> roles = Db.find("select * from roles");
/* 125 */     Record layTableRes = new Record();
/* 126 */     layTableRes.set("code", Integer.valueOf(0));
/* 127 */     layTableRes.set("msg", "");
/* 128 */     layTableRes.set("count", Integer.valueOf(roles.size()));
/* 129 */     layTableRes.set("data", roles);
/* 130 */     renderJson(layTableRes);
/*     */   }
/*     */   
/*     */   public void getMenuTree() {
/* 134 */     Record rsp = new Record();
/* 135 */     rsp.set("code", Integer.valueOf(0));
/* 136 */     rsp.set("msg", "获取成功");
/* 137 */     Record trees = new Record();
/* 138 */     trees.set("trees", getMenu(getParaToInt(0).intValue()));
/* 139 */     rsp.set("data", trees);
/* 140 */     renderJson(rsp);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   @Before({Tx.class})
/*     */   public void roleUpdate()
/*     */   {
/* 148 */     String menuIds = getPara("role_menu_auth");
/* 149 */     int roleId = getParaToInt("role_id").intValue();
/* 150 */     Record role = new Record();
/* 151 */     if (roleId != 0) {
/* 152 */       role = Db.findById("roles", Integer.valueOf(roleId));
/*     */     }
/* 154 */     role.set("role_name", getPara("role_name"));
/* 155 */     role.set("role_nick_name", getPara("role_nick_name"));
/* 156 */     role.set("role_desc", getPara("role_desc"));
/* 157 */     if (roleId != 0) {
/* 158 */       Db.update("roles", role);
/*     */     } else {
/* 160 */       Db.save("roles", role);
/*     */     }
/*     */     
/* 163 */     clearMenuAuth(role.getInt("id").intValue());
/*     */     
/* 165 */     String[] menuIdArray = menuIds.split(",");
/* 166 */     for (int i = 0; i < menuIdArray.length; i++) {
/* 167 */       Record permission = new Record();
/* 168 */       permission.set("type", Integer.valueOf(1));
/* 169 */       permission.set("gl_id", menuIdArray[i]);
/* 170 */       Db.save("permissions", permission);
/* 171 */       Record rolePermission = new Record();
/* 172 */       rolePermission.set("role_id", role.getInt("id"));
/* 173 */       rolePermission.set("permission_id", permission.get("id"));
/* 174 */       Db.save("role_permission", rolePermission);
/*     */     }
/* 176 */     Record rsp = new Record();
/* 177 */     rsp.set("code", Integer.valueOf(0));
/* 178 */     rsp.set("msg", "角色及权限新增或修改成功");
/* 179 */     renderJson(rsp);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void clearMenuAuth(int roleId)
/*     */   {
/* 186 */     List<Record> permissions = Db.find("SELECT * FROM permissions WHERE TYPE = 1 AND id IN (SELECT permission_id FROM role_permission WHERE role_id = " + roleId + ")");
/* 187 */     for (int i = 0; i < permissions.size(); i++) {
/* 188 */       int perId = ((Integer)((Record)permissions.get(i)).get("id")).intValue();
/* 189 */       Db.deleteById("permissions", Integer.valueOf(perId));
/* 190 */       Db.delete("delete from role_permission where role_id = ? and permission_id = ?", new Object[] { Integer.valueOf(roleId), Integer.valueOf(perId) });
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Record> getMenu(int roleId)
/*     */   {
/* 199 */     List<Record> top_menu = Db.find("select * from menu where menu_level = 1 and is_hide = 0");
/* 200 */     top_menu = menuAuth(top_menu, roleId);
/* 201 */     for (int i = 0; i < top_menu.size(); i++) {
/* 202 */       List<Record> second_menu = Db.find("select * from menu where menu_level = 2 and is_hide = 0 and parent_menu = " + ((Record)top_menu.get(i)).getInt("id") + " order by seq_num");
/* 203 */       second_menu = menuAuth(second_menu, roleId);
/* 204 */       for (int j = 0; j < second_menu.size(); j++) {
/* 205 */         List<Record> third_menu = Db.find("select * from menu where menu_level = 3 and is_hide=0 and parent_menu = " + ((Record)second_menu.get(j)).getInt("id") + " order by seq_num");
/* 206 */         third_menu = menuAuth(third_menu, roleId);
/* 207 */         if (third_menu.size() > 0) {
/* 208 */           ((Record)second_menu.get(j)).set("list", third_menu);
/*     */         }
/*     */       }
/* 211 */       if (second_menu.size() > 0) {
/* 212 */         ((Record)top_menu.get(i)).set("list", second_menu);
/*     */       }
/*     */     }
/* 215 */     return top_menu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Record> menuAuth(List<Record> menu, int roleId)
/*     */   {
/* 223 */     String sql = "";
/* 224 */     if (roleId != 0) {
/* 225 */       sql = "SELECT gl_id FROM permissions WHERE id IN (SELECT permission_id FROM role_permission WHERE role_id = " + roleId + ") AND TYPE = 1";
/*     */     } else {
/* 227 */       sql = "SELECT gl_id FROM permissions WHERE id IN (SELECT permission_id FROM role_permission) AND TYPE = 1";
/*     */     }
/* 229 */     List<Record> menuPermissions = Db.find(sql);
/* 230 */     for (int i = 0; i < menu.size(); i++) {
/* 231 */       boolean flag = false;
/* 232 */       for (int j = 0; j < menuPermissions.size(); j++) {
/* 233 */         if (((Record)menu.get(i)).getInt("id") == ((Record)menuPermissions.get(j)).getInt("gl_id")) {
/* 234 */           flag = true;
/*     */         }
/*     */       }
/* 237 */       if (flag) {
/* 238 */         ((Record)menu.get(i)).set("checked", Boolean.valueOf(true));
/*     */       } else {
/* 240 */         ((Record)menu.get(i)).set("checked", Boolean.valueOf(false));
/*     */       }
/* 242 */       ((Record)menu.get(i)).set("name", ((Record)menu.get(i)).get("title"));
/* 243 */       ((Record)menu.get(i)).set("value", ((Record)menu.get(i)).get("id"));
/*     */     }
/* 245 */     return menu;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void menu()
/*     */   {
/* 252 */     render("menu.html");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void getTreeMenu()
/*     */   {
/* 259 */     List<Record> menu = Db.find("select * from menu order by seq_num");
/* 260 */     Record rsp = new Record();
/* 261 */     rsp.set("code", Integer.valueOf(0));
/* 262 */     rsp.set("count", Integer.valueOf(menu.size()));
/* 263 */     rsp.set("data", menu);
/* 264 */     renderJson(rsp);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void menuAddOrEdit()
/*     */   {
/* 271 */     int menuId = getParaToInt(0).intValue();
/* 272 */     int parent_menu = getParaToInt(1).intValue();
/* 273 */     setAttr("menu", new Record());
/* 274 */     if (menuId != 0) {
/* 275 */       Record menu = Db.findById("menu", Integer.valueOf(menuId));
/* 276 */       setAttr("menu", menu);
/*     */     }
/* 278 */     if (parent_menu == 0) {
/* 279 */       setAttr("menu_level", Integer.valueOf(1));
/*     */     } else {
/* 281 */       Record parentMenu = Db.findById("menu", Integer.valueOf(parent_menu));
/* 282 */       setAttr("menu_level", Integer.valueOf(parentMenu.getInt("menu_level").intValue() + 1));
/*     */     }
/* 284 */     setAttr("parent_menu", Integer.valueOf(parent_menu));
/* 285 */     render("menuform.html");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void doMenuAddOrEdit()
/*     */   {
/* 292 */     Menu menu = (Menu)getModel(Menu.class, "");
/* 293 */     menu.set("is_hide", Integer.valueOf(0));
/* 294 */     if (menu.getInt("id") != null) {
/* 295 */       menu.update();
/*     */     } else {
/* 297 */       menu.save();
/*     */     }
/* 299 */     Record rsp = new Record();
/* 300 */     rsp.set("code", Integer.valueOf(0));
/* 301 */     rsp.set("msg", "添加或修改菜单成功");
/* 302 */     renderJson(rsp);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void delMenu()
/*     */   {
/* 309 */     deleteMenu(getParaToInt("id").intValue());
/* 310 */     Record rsp = new Record();
/* 311 */     rsp.set("code", Integer.valueOf(0));
/* 312 */     rsp.set("msg", "删除菜单及子菜单成功");
/* 313 */     renderJson(rsp);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void deleteMenu(int parent_menu)
/*     */   {
/* 320 */     List<Menu> sonMenus = Menu.dao.find("select * from menu where parent_menu = " + parent_menu);
/* 321 */     for (int i = 0; i < sonMenus.size(); i++) {
/* 322 */       deleteMenu(((Menu)sonMenus.get(i)).getInt("id").intValue());
/*     */     }
/* 324 */     Menu.dao.deleteById(Integer.valueOf(parent_menu));
/*     */   }
/*     */ }


/* Location:              C:\Users\Ray\Desktop\ERP.war!\WEB-INF\classes\com\ray\controller\SysController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */