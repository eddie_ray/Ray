/*    */ package com.ray.shiro;
/*    */ 
/*    */ import com.jfinal.plugin.activerecord.Db;
/*    */ import com.jfinal.plugin.activerecord.Record;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Collection;
/*    */ import java.util.List;
/*    */ import org.apache.shiro.authc.AuthenticationException;
/*    */ import org.apache.shiro.authc.AuthenticationInfo;
/*    */ import org.apache.shiro.authc.AuthenticationToken;
/*    */ import org.apache.shiro.authc.SimpleAuthenticationInfo;
/*    */ import org.apache.shiro.authc.UsernamePasswordToken;
/*    */ import org.apache.shiro.authz.AuthorizationInfo;
/*    */ import org.apache.shiro.authz.SimpleAuthorizationInfo;
/*    */ import org.apache.shiro.cache.Cache;
/*    */ import org.apache.shiro.realm.AuthorizingRealm;
/*    */ import org.apache.shiro.subject.PrincipalCollection;
/*    */ import org.apache.shiro.subject.SimplePrincipalCollection;
/*    */ 
/*    */ public class ShiroDbRealm
/*    */   extends AuthorizingRealm
/*    */ {
/*    */   protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
/*    */     throws AuthenticationException
/*    */   {
/* 27 */     UsernamePasswordToken token = (UsernamePasswordToken)authcToken;
       /*Record user = Db.findFirst("select * from user where username = ?", new Object[] { token.getUsername() });
 29      if (user != null) {
 30        return new SimpleAuthenticationInfo(user.get("username"), user.get("password"), getName());
         }	
 32      return null;*/

			return new SimpleAuthenticationInfo(token.getUsername(), token.getPassword(), getName());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals)
/*    */   {
/* 40 */     Collection<String> roleNameList = new ArrayList();
/* 41 */     Collection<String> permissionNameList = new ArrayList();
/* 42 */     String loginName = (String)principals.fromRealm(getName()).iterator().next();
/* 43 */     Record user = Db.findFirst("select * from user where username = ?", new Object[] { loginName });
/* 44 */     if (user != null) {
/* 45 */       SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
/* 46 */       List<Record> roleList = Db.find("select * from user_role where user_id = ?", new Object[] { user.getInt("id") });
/* 47 */       for (int i = 0; i < roleList.size(); i++) {
/* 48 */         Record role = Db.findById("roles", ((Record)roleList.get(i)).get("role_id"));
/* 49 */         roleNameList.add(role.getStr("role_name"));
/* 50 */         List<Record> permissionList = Db.find("select * from role_permission where role_id = ?", new Object[] { ((Record)roleList.get(i)).getInt("role_id") });
/* 51 */         for (int j = 0; j < permissionList.size(); j++) {
/* 52 */           Record permission = Db.findById("permissions", ((Record)permissionList.get(j)).getInt("permission_id"));
/*    */           
/* 54 */           if (permission.getInt("type").intValue() == 2) {
/* 55 */             permissionNameList.add(permission.getStr("permission_name"));
/*    */           }
/*    */         }
/*    */       }
/* 59 */       info.addRoles(roleNameList);
/* 60 */       info.addStringPermissions(permissionNameList);
/* 61 */       return info;
/*    */     }
/* 63 */     return null;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void clearCachedAuthorizationInfo(String principal)
/*    */   {
/* 71 */     SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
/* 72 */     clearCachedAuthorizationInfo(principals);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void clearAllCachedAuthorizationInfo()
/*    */   {
/* 79 */     Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
/* 80 */     if (cache != null) {
/* 81 */       for (Object key : cache.keys()) {
/* 82 */         cache.remove(key);
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Ray\Desktop\ERP.war!\WEB-INF\classes\com\ray\shiro\ShiroDbRealm.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */