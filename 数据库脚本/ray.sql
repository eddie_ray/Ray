/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.13 : Database - biqs
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`biqs` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;

USE `biqs`;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_level` int(11) NOT NULL COMMENT '菜单层级',
  `parent_menu` int(11) NOT NULL COMMENT '父级菜单',
  `title_en` varchar(255) NOT NULL COMMENT '菜单英文',
  `title` varchar(255) NOT NULL COMMENT '菜单名称',
  `seq_num` int(11) NOT NULL COMMENT '菜单序号',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `href` varchar(255) DEFAULT NULL COMMENT '菜单链接',
  `is_hide` int(1) NOT NULL COMMENT '是否隐藏：1隐藏，0显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`id`,`menu_level`,`parent_menu`,`title_en`,`title`,`seq_num`,`icon`,`href`,`is_hide`) values (1,1,0,'system','系统设置',3,'ray-shezhi',NULL,0),(3,2,1,'role','角色管理',1,NULL,'/sys/role',0),(4,2,1,'menu','菜单管理',2,NULL,'/sys/menu',0),(5,2,1,'org','组织架构',0,NULL,'/os',0);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '权限名称',
  `permission_name` varchar(255) DEFAULT NULL COMMENT '权限表达式',
  `type` int(1) NOT NULL COMMENT '权限类型：1菜单，2按钮，3数据',
  `gl_id` int(11) DEFAULT NULL COMMENT '关联菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=792 DEFAULT CHARSET=utf8;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`permission_name`,`type`,`gl_id`) values (785,NULL,NULL,1,1),(786,NULL,NULL,1,5),(787,NULL,NULL,1,3),(788,NULL,NULL,1,4),(791,NULL,NULL,1,1);

/*Table structure for table `role_permission` */

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `permission_id` int(11) NOT NULL COMMENT '权限ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=796 DEFAULT CHARSET=utf8;

/*Data for the table `role_permission` */

insert  into `role_permission`(`id`,`role_id`,`permission_id`) values (789,1,785),(790,1,786),(791,1,787),(792,1,788),(795,13,791);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_name` varchar(255) NOT NULL COMMENT '角色',
  `role_nick_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '具体描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_name`,`role_nick_name`,`role_desc`) values (1,'admin','管理员','所有权限'),(13,'basic','普通用户','默认权限');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` varchar(255) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=455 DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`user_id`,`role_id`) values (451,'09112815001228979',1),(452,'193125692825944092',1),(453,'0316374868950344',1),(454,'155807442525988253',13);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
